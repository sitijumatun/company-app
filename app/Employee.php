<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Employee extends Model
{
    protected $fillable = [
      'name',
      'email'
    ];

    /**
     * @return BelongsTo
     */
    public function company(){
        return $this->belongsTo(Company::class);
    }
}
