<?php

namespace App\Http\Controllers\Web;

use App\Company;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;
use Illuminate\View\View;
use Intervention\Image\Facades\Image;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Application|Factory|View
     */
    public function index(Request $request)
    {
        $companies = Company::when($request->has('q'), static function(Builder $query) use ($request){
                return $query->where('name', 'like', '%'.$request->get('q').'%');
            })
            ->orderBy('id')
            ->paginate(5);
        return view ('companies.index', compact('companies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View
     */
    public function create()
    {
        return view('companies.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     * @throws ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'website' => 'required',
            'logo' => 'required|dimensions:min_width=100,min_height=200|mimes:png|max:2048'
        ]);

        $image = $request->file('logo');
        $images = time() . '.' . $image->getClientOriginalExtension();
        Image::make($image)->save(storage_path('app/public/company/' . $images));

        $input = [
           'name' => $request->get('name'),
           'email' => $request->get('email'),
           'website' => $request->get('website'),
           'logo' => $images
        ];

        $company = new Company();
        $company->fill($input);
        $company->save();

        return redirect()->route('company.index')
            ->with('success', 'Company data is successfully saved');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param Company $company
     * @return Application|Factory|View
     */
    public function edit(Company $company)
    {
        return view('companies.edit', compact('company'));
    }


    /**
     * @param Request $request
     * @param Company $company
     * @throws ValidationException
     */
    public function update(Request $request, Company $company)
    {
        $rules = [
            'name' => 'required',
            'email' => 'required',
            'website' => 'required',
        ];
        if($request->has('logo')){
            $rules +=[
                'logo' => 'required|dimensions:min_width=100,min_height=200|mimes:png|max:2048'
            ];
        }
        $this->validate($request, $rules);

        $input = [
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'website' => $request->get('website')

        ];

        if($request->has('logo')) {
            $image = $request->file('logo');
            $images = time() . '.' . $image->getClientOriginalExtension();
            Image::make($image)->save(storage_path('app/public/company/' . $images));
            $input +=[
                'logo' => $images
            ];
        }

        $company->fill($input);
        $company->update();

        return redirect()->route('company.index')
            ->with('success', 'Company data is successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return RedirectResponse
     * @throws Exception
     */
    public function destroy(Company $company)
    {
        $company->delete();

        return redirect()->route('company.index')
            ->with('success', 'Company data is successfully deleted');
    }
}
