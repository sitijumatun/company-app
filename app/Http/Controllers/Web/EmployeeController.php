<?php

namespace App\Http\Controllers\Web;

use App\Company;
use App\Employee;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Illuminate\View\View;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index(Request $request)
    {
        $employees = Employee::when($request->has('q'), static function(Builder $query) use ($request){
            return $query->where('name','like','%'.$request->get('q').'%');
        })
            ->orderBy('created_at')
            ->paginate(5);
        return view('employees.index', compact('employees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View
     */
    public function create()
    {
        $companies = Company::orderBy('name')->get();
        return view('employees.create', compact('companies'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     * @throws ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'company_id' => 'required'
        ]);

        $employee = new Employee();
        $employee->fill($request->all());
        $employee->company()->associate($request->get('company_id'));
        $employee->save();

        return redirect()->route('employee.index')
            ->with('success', 'Employee data is successfully saved');

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Application|Factory|View
     */
    public function edit(Employee $employee)
    {
        $companies = Company::orderBy('name')->get();
        return view('employees.edit', compact('companies', 'employee'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return RedirectResponse
     * @throws ValidationException
     */
    public function update(Request $request, Employee $employee)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'company_id' => 'required'
        ]);

        $employee->fill($request->all());
        $employee->company()->associate($request->get('company_id'));
        $employee->update();

        return redirect()->route('employee.index')
            ->with('success', 'Employee data is successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return RedirectResponse
     * @throws Exception|Exception
     */
    public function destroy(Employee $employee)
    {
        $employee->delete();

        return redirect()->route('employee.index')
            ->with('success', 'Employee data is successfully deleted');
    }
}
