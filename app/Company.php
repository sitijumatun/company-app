<?php

namespace App;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\UrlGenerator;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Company extends Model
{
    protected $appends = ['logo_url'];
    protected $fillable = [
      'name',
      'email',
      'logo',
      'website'
    ];

    /**
     * @return Application|UrlGenerator|string|null
     */
    public function getLogoUrlAttribute(){
        if(isset($this->attributes['logo'])) {
            return url(asset('storage/company/'.$this->attributes['logo']));
        }

        return null;
    }

    /**
     * @return HasMany
     */
    public function employees(){
        return $this->hasMany(Employee::class);
    }


}
