@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Management Employee</div>
                    @if(session()->get('success'))
                        <div class="alert alert-success"style="margin: 20px">
                            {{ session()->get('success') }}
                        </div><br />
                    @endif
                    <div class="card-body">
                        <a href="{{ route('employee.create') }}" class="btn btn-primary">Add Data</a>
                        <div class="row" style="margin:20px">
                            <form class="row">
                                <div class="input-group hidden-xs" style="width: 150px;">
                                    <input class="form-control" placeholder="{{ __('Search') }}&hellip;" name="q" value="{{ request('q') }}">
                                </div>
                            </form>
                        </div>
                        @if(!$employees->isEmpty())
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <td>ID</td>
                                    <td>Name</td>
                                    <td>Email</td>
                                    <td>Company</td>
                                    <td width="150px">Action</td>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($employees as $employee)
                                    <tr>
                                        <td>{{ $employee->id }}</td>
                                        <td>{{ $employee->name }}</td>
                                        <td>{{ $employee->email }}</td>
                                        <td>{{ $employee->company->name }}</td>
                                        <td>
                                            <div class="btn-group">
                                                <a data-toggle="modal" data-target="#myModal{{ $employee->id }}" class="btn btn-warning">Show</a>
                                                <!-- Modal -->
                                                <div id="myModal{{ $employee->id }}" class="modal fade" role="dialog">
                                                    <div class="modal-dialog">

                                                        <!-- Modal content-->
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <table class="table table-striped">
                                                                    <thead>
                                                                    <tr>
                                                                        <td><b>Name</b></td>
                                                                        <td>{{ $employee->name }}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><b>Email</b></td>
                                                                        <td>{{ $employee->email }}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><b>Company</b></td>
                                                                        <td>{{ $employee->company->name }}</td>
                                                                    </tr>
                                                                    </thead>
                                                                </table>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                                <a href="{{ route('employee.edit', $employee->id)}}" class="btn btn-primary">Edit</a>
                                                <form action="{{ route('employee.destroy', $employee->id)}}" method="post">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button class="btn btn-danger" type="submit">Delete</button>
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <center>{{ $employees->links() }}</center>
                        @else
                            <div class="px-3 text-center" style="padding: 10px">
                                <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                                    No Data
                                </p>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
