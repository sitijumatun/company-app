@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Management Company</div>
                @if(session()->get('success'))
                    <div class="alert alert-success"style="margin: 20px">
                        {{ session()->get('success') }}
                    </div><br />
                @endif
                <div class="card-body">
                    <a href="{{ route('company.create') }}" class="btn btn-primary">Add Data</a>
                    <div class="row" style="margin:20px">
                        <form class="row">
                            <div class="input-group hidden-xs" style="width: 150px;">
                                <input class="form-control" placeholder="{{ __('Search') }}&hellip;" name="q" value="{{ request('q') }}">
                            </div>
                        </form>
                    </div>
                    @if(!$companies->isEmpty())
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <td>ID</td>
                            <td>Logo</td>
                            <td>Name</td>
                            <td>Email</td>
                            <td>Website</td>
                            <td width="150px">Action</td>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($companies as $company)
                        <tr>
                            <td>{{ $company->id }}</td>
                            <td>
                                <img src="{{ $company->logo_url }}" width="100px" height="100px" alt="{{ $company->logo }}">
                            </td>
                            <td>{{ $company->name }}</td>
                            <td>{{ $company->email }}</td>
                            <td>{{ $company->website }}</td>
                            <td>
                                <div class="btn-group">
                                    <a data-toggle="modal" data-target="#myModal{{ $company->id }}" class="btn btn-warning">Show</a>
                                    <!-- Modal -->
                                    <div id="myModal{{ $company->id }}" class="modal fade" role="dialog">
                                        <div class="modal-dialog">

                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                </div>
                                                <div class="modal-body">
                                                    <table class="table table-striped">
                                                        <thead>
                                                        <tr>
                                                            <td></td>
                                                            <td><img src="{{ $company->logo_url }}" width="200px" height="200px" alt="{{ $company->logo }}"></td>
                                                        </tr>
                                                        <tr>
                                                            <td><b>Name</b></td>
                                                            <td>{{ $company->name }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td><b>Email</b></td>
                                                            <td>{{ $company->email }}</td>
                                                        </tr>
                                                        <tr>
                                                            <td><b>Website</b></td>
                                                            <td>{{ $company->website }}</td>
                                                        </tr>
                                                        </thead>
                                                    </table>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <a href="{{ route('company.edit', $company->id)}}" class="btn btn-primary">Edit</a>
                                    <form action="{{ route('company.destroy', $company->id)}}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-danger" type="submit">Delete</button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <center>{{ $companies->links() }}</center>
                    @else
                        <div class="px-3 text-center" style="padding: 10px">
                            <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
                                No Data
                            </p>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
