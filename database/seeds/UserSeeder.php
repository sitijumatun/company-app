<?php

use App\Constants\Role;
use App\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->fill([
            'name' => 'admin',
            'email' =>  'admin@transisi.id',
            'role' => Role::ADMINISTRATOR,
            'password' => bcrypt('password')
        ]);
        $user->save();
    }
}
