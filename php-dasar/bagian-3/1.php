<?php
$arr = [
    ['f', 'g', 'h', 'i'],
    ['j', 'k', 'p', 'q'],
    ['r', 's', 't', 'u']
];

function cari(array $arrayData, string $stringSearch){
    $result = 'false';
    $stringSearchArray = str_split($stringSearch);
    $sumSearch = count($stringSearchArray);
    $sumResultSearch = 0;
    $arrayCollect = array();
    foreach ($arrayData as $arrayDatum) {
        foreach ($arrayDatum as $data) {
            array_push($arrayCollect, $data);
        }
    }

    foreach ($stringSearchArray as $searchData) {
            if(in_array($searchData, $arrayCollect)){
                $sumResultSearch +=1;
            }
    }

    if($sumResultSearch == $sumSearch){
        $result = 'true';
    }

    return $result;
}

echo 'hasil dari cari($arr, \'fghi\') hasilnya : '. cari($arr, 'fghi').'<br/>';
echo 'hasil dari cari($arr, \'fghp\') hasilnya : '. cari($arr, 'fghp').'<br/>';
echo 'hasil dari cari($arr, \'fjrstp\') hasilnya : '. cari($arr, 'fjrstp');
