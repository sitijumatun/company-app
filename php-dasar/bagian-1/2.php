<?php

function checkLowerCaseCount(array $inputData){
    $sum = 0;
    foreach ($inputData as $data) {
        if(ctype_lower($data)){
            $sum+=1;
        }
    }

    return $sum;
}

$input = "TranSISI";
$inputArray = str_split($input);
echo "'".$input."' mengandung ".checkLowerCaseCount($inputArray)." buah huruf kecil";
