<?php
function unigram(array $inputData){
    return implode(', ',$inputData);
}

function bigram(array $inputData){
    $result = array();
    for($i = 1 ; $i <= count($inputData); $i++){
        if($i % 2 !== 0) {
            $data = $inputData[$i-1] . ' ' . $inputData[$i];
            array_push($result, $data);
        }
    }

    return implode(', ',$result);

}

function trigram(array $inputData){
    $result = array();
    for($i = 1 ; $i <= count($inputData); $i++){
        if($i % 3 === 0) {
            $data = $inputData[$i-3] . ' ' . $inputData[$i-2]. ' '.$inputData[$i-1];
            array_push($result, $data);
        }
    }

    return implode(', ',$result);
}


$input = 'Jakarta adalah ibukota negara Republik Indonesia';
$inputArray = explode(' ', $input);
echo 'Unigram : '.unigram($inputArray).'<br/>';
echo 'Bigram : '.bigram($inputArray).'<br/>';
echo 'Trigram : '.trigram($inputArray);
