<?php

function searchAverage(Array $inputData) {
    $sum = 0;
    $count = count($inputData);
    foreach ($inputData as $nilai) {
        $sum += (int) $nilai;
    }

    $sum = $sum / $count;
    return round($sum, 2);
}

function searchSevenMin(Array $inputData){
    $data = array();
    $result = array();
    asort($inputData);
    $arrayUnique = array_unique($inputData);
    foreach ($arrayUnique as $array) {
        array_push($data, $array);
    }

    for($i = 0 ; $i < 7 ; $i++){
        $result[$i] = $data[$i];
    }

    return implode(' ', $result);
}

function searchSevenMax(Array $inputData){
    $data = array();
    $result = array();
    rsort($inputData);
    $arrayUnique = array_unique($inputData);

    foreach ($arrayUnique as $array) {
        array_push($data, $array);
    }

    for($i = 0 ; $i < 7 ; $i++){
        $result[$i] = $data[$i];
    }

    return implode(' ', $result);

}

$nilai ="72 65 73 78 75 74 90 81 87 65 55 69 72 78 79 91 100 40 67 77 86";
$arrayNilai = explode(' ', $nilai);
echo "7 Terendah : ".searchSevenMin($arrayNilai)."<br/>";
echo "7 Tertinggi : ".searchSevenMax($arrayNilai)."<br/>";
echo "Nilai rata rata : ".searchAverage($arrayNilai);




