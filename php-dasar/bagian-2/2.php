<?php

function letterToNumber($letter) {
    $alphabets = range('A', 'Z');
    return array_search($letter, $alphabets) + 1;
}

function numberToLetter($number) {
    $alphabets = range('A', 'Z');
    return $alphabets[$number-1];
}


function encryption($strInput) {
    $result = "";
    $arrInput = str_split($strInput);
    $count = 0;
    foreach ($arrInput as $idx => $letter) {
        $count++;
        $letterNumber = letterToNumber($letter);
        if((($idx+1) % 2) == 0){
            $result .= numberToLetter($letterNumber - $count);
        } else {
            $result .= numberToLetter($letterNumber + $count);
        }
    }

    return $result;
}

$input = 'DFHKNQ';
echo 'input : '.$input. '<br/> hasil enkripsi : ' .encryption($input);
